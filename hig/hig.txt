[[contributor-notes]]
= Human Interface Guidelines

// Chapters should be defined here, and each one should include the individual
// topic files that make up the chapter, in order.

[[preface]]
== Preface

include::copyright.txt[]

[[patterns-chapter]]
== Interface Patterns

A design pattern captures architectural design ideas as archetypal and reusable
descriptions, providing a reusable approach to solving common design problems.

The Drupal administrative interface has to solve some recurring UI problems
too. This section contains Drupal user interface design patterns. Use them
to create and maintain consistency in the overall Drupal user experience.

[[groupings-section]]
=== Grouping elements

include::details.txt[]
include::vertical-tabs.txt[]

[[appendix]]
[appendix]
== Appendix

include::attributions.txt[]
