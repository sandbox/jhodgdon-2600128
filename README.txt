This is a sandbox project, to see whether it makes sense to maintain and
display the Human Interface Guidelines documentation using AsciiDoc and
the https://www.drupal.org/project/asciidoc_display module.

What follows is the README for the eventual plan. It's not done yet.

----------

The files in this project can be used to build a web site displaying the Human
Interface Guidelines for the Drupal project. The source uses AsciiDoc markdown
format, which can be compiled into DocBook format, which in turn can be compiled
into HTML to be displayed with the AsciiDoc Display module
(https://www.drupal.org/project/asciidoc_display).


COPYRIGHT AND LICENSE
---------------------

See the ASSETS.yml file in this directory, and files it references, for
copyright and license information for the text source files and images in this
project. Output files produced and displayed by this project also must contain
copyright/license information.

Code (scripts) in this project are licensed under the GNU/GPL version 2 and
later license.


FILE ORGANIZATION
-----------------

This project contains the following directories:

* hig

Source code for the HIG documentation.

* templates

Templates for documentation pages.

* scripts

Scripts used to build the documentation.
