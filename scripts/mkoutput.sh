# This script builds the AsciiDoc Display module output for the HIG.

# Exit immediately on uninitialized variable or error, and print each command.
set -uex

# Make the output directories if they do not exist. Make sure you have a
# separate directory for each book you want to display, and each language.
mkdir -p ../output
mkdir -p ../output/html
mkdir -p ../output/html/images
mkdir -p ../output/html/iframes

# Run the preprocessor that puts file names into the files under each header.
# Only for the special HTML output.
php addnames._php ../hig ../output/html

# Copy image and HTML files to output directory.
# cp ../hig/images/*.png ../output/html/images
cp ../hig/iframes/*.html ../output/html/iframes

# Run the AsciiDoc processor to convert to DocBook format, for the special
# HTML output. The syntax is:
#  asciidoc -d book -b docbook -f [config file] -o [output file] [input file]
asciidoc -d book -b docbook -f std.conf -o ../output/html/hig.docbook ../output/html/hig.txt

# Run the xmlto processor to convert from DocBook to bare XHTML, using a custom
# style sheet that makes output this module can recognize.  The syntax is:
#   xmlto -m bare.xsl xhtml -o [output dir] [input docbook file]
xmlto -m bare.xsl xhtml --stringparam section.autolabel.max.depth=2 --stringparam chunk.section.depth=2 --stringparam chunk.first.sections=1 -o ../output/html ../output/html/hig.docbook

